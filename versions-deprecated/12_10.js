import flux from "@aust/react-flux";

flux.dispatch("version/add", {
  major: 12,
  minor: 10,
});
