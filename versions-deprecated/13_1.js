import flux from "@aust/react-flux";

flux.dispatch("version/add", {
  major: 13,
  minor: 1,
});
